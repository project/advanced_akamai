
-- SUMMARY --

The Akamai modules provides integration with the Akamai Content Control Utility
 (CCU) Web Service

-- REQUIREMENTS --
  No

-- INSTALLATION --
* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONTACT --
Current maintainers:
* Ankush Gautam https://www.drupal.org/u/agautam
  email : ankushgautam76@gmail.com
* Ishwar chandra Tiwari https://www.drupal.org/u/ishwar
  email : ishwar.tec@gmail.com
